#include <iostream>
#include "Sorts.h"
#include <time.h>

using namespace std;



int main()
{
	int a[10000];

	for (int i = 0; i < 10000; ++i)
		a[i] = (rand() % 400);


	clock_t startT, endT;

	startT = clock();

	Sorts::quicksort(a, 0, 100 - 1);
	endT = clock();

	float dif = (float)endT - (float)startT;

	cout << dif << "\n";

	system("Pause");

	return 0;

}