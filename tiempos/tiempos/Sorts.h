#pragma once
#include <iostream>

class Sorts
{
public:
	static void BoobleSort(int a[]);

	static void quicksort(int *array, int start, int end);

	static void mergeSort(int arr[], int l, int r);

private:
	static int divide(int *array, int start, int end);
	static void merge(int arr[], int l, int m, int r);

};