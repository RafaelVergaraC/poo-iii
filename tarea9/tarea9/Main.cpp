#include <iostream>
#include "Heap.h"

int main()
{
	Heap z;

	int x[]
	{
		5,3,6,8,1,10,6,75
	};


	int len = sizeof(x);

	for (int i = 0; i < len; ++i)
		z.insert(x[i]);


	while (z.list.size() > 0)
		std::cout << "Heap Max\t" << z.remove() << std::endl;

	system("pause");

	return 0;

}