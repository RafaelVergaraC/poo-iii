#include "Heap.h"



Heap::Heap()
{
}


Heap::~Heap()
{
}

void Heap::insert(int a)
{
	list.push_back(a);
	changePositionUp();
}

void Heap::intercambiar(int hijo, int padre)
{
	int temp;
	temp = list[hijo];
	list[hijo] = list[padre];
	list[padre] = temp;
}

void Heap::changePositionUp()
{
	int hijo = list.size() - 1;
	int padre = getPadre(hijo);

	while (list[hijo] > list[padre] && hijo >= 0 && padre >= 0)
	{
		intercambiar(hijo, padre);
		hijo = padre;
		padre = getPadre(hijo);
	}
}

void Heap::changePositionDown()
{
	int padre = 0;

	while (1) {
		int left = getHijoIzquierdo(padre);
		int right = getHijoDerecho(padre);
		int length = list.size();
		int largest = padre;

		if (left < length && list[left] > list[largest])
			largest = left;

		if (right < length && list[right] > list[largest])
			largest = right;

		if (largest != padre) {
			intercambiar(largest, padre);
			padre = largest;
		}
		else
			break;


	}
}

int Heap::getPadre(int hijo)
{

	if (hijo % 2 == 0)
		return (hijo / 2) - 1;
	else
		return hijo / 2;
}

int Heap::getHijoDerecho(int padre)
{
	return 2 * padre + 1;
}

int Heap::getHijoIzquierdo(int padre)
{
	return 2 * padre + 2;
}

int Heap::remove()
{
	int hijo = list.size() - 1;
	intercambiar(hijo, 0);

	int value = list.back();
	list.pop_back();
	changePositionDown();


	return value;
}