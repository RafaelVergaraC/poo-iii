#pragma once
#include <iostream>
#include <vector>

class Heap
{
public:
	std::vector<int> list;


	Heap();
	~Heap();

	void insert(int a);
	void intercambiar(int hijo, int padre);

	void changePositionUp();
	void changePositionDown();

	int  getPadre(int hijo);
	int  getHijoDerecho(int padre);
	int  getHijoIzquierdo(int padre);

	int remove();
};