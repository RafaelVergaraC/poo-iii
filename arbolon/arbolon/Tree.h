#pragma once
#include <iostream>


class Tree
{
public:

	Tree();
	~Tree();

	int value;
	Tree *izq, *der;

	static Tree* crearNodo(int x);
	void insertar(Tree *& arbol, int x);
	static void Draw(Tree *arbol, int x);
};
