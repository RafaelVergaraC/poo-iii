#include "Tree.h"



Tree::Tree()
{
}


Tree::~Tree()
{
}

Tree* Tree::crearNodo(int x)
{
	Tree* nuevoNodo = new Tree();
	nuevoNodo->value = x;
	nuevoNodo->der = nullptr;
	nuevoNodo->izq = nullptr;

	return nuevoNodo;

}

void Tree::insertar(Tree * & arbol, int x)
{
	if (arbol == nullptr)
	{
		arbol = crearNodo(x);
	}

	else if (x < arbol->value)
		insertar(arbol->izq, x);

	else if (x > arbol->value)
		insertar(arbol->der, x);


}

void Tree::Draw(Tree * arbol, int x)
{

	if (arbol == nullptr)
		return;
	Draw(arbol->der, x + 1);

	for (int i = 0; i < x; ++i)
		std::cout << "   ";

	std::cout << arbol->value << "\n";

	Draw(arbol->izq, x + 1);



}