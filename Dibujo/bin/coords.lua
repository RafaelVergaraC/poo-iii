function circle()
	local x = 300
	local y = 300
	local radius = 150
	return x,y,radius
end

function quad()
	local x = 600
	local y = 200
	local w = 250
	local h = 250
	return x,y,w,h
end

function secondQuad()
	local x = 300
	local y = 500
	local w = 500
	local h = 200
	return x,y,w,h
end