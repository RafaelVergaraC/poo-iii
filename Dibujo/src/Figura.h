#pragma once
#include "lua.hpp"
#include "ofGraphics.h"
#include "ofAppRunner.h"

class Figura {
public:
	string figura;
	int x;
	int y;
	int width;
	int height;
	int radius;
	int id;
	char *value;

	bool circleSelect;
	bool quadSelect;
	bool rectSelect;

	Figura();
	~Figura();
	void circle(lua_State *file);
	void quad(lua_State *file);
	void secondQuad(lua_State *file);
	int hash(char *clave);
	void start(string _figura, char *_value);
	void draw();
};