#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	quad.start("cuadrado", "cuadrad�n prron");
	circle.start("circulo", "circul�n prron");
	rect.start("rectangulo", "rectangul�n prron");
}

//--------------------------------------------------------------
void ofApp::update(){

}

//--------------------------------------------------------------
void ofApp::draw(){
	quad.draw();
	circle.draw();
	rect.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
	if (button == OF_MOUSE_BUTTON_1) {
		if (x < circle.x + circle.radius) {
			if (x > circle.x - circle.radius) {
				if (y < circle.y + circle.radius) {
					if (y > circle.y - circle.radius) {
						circle.circleSelect = true;
						quad.quadSelect = false;
						rect.rectSelect = false;
					}
				}
			}
		}else if (x > quad.x) {
			if (x < quad.x + quad.width) {
				if (y > quad.y) {
					if (y < quad.y + quad.height) {
						circle.circleSelect = false;
						quad.quadSelect = true;
						rect.rectSelect = false;
					}
				}
			}
		} else if (x > rect.x) {
			if (x < rect.x + rect.width) {
				if (y > rect.y) {
					if (y < rect.y + rect.height) {
						circle.circleSelect = false;
						quad.quadSelect = false;
						rect.rectSelect = true;
					}
				}
			}
		}
	}
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
