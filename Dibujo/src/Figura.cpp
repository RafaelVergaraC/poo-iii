#include "Figura.h"

Figura::Figura(){
	x = 0;
	y = 0;
	width = 0;
	height = 0;
	radius = 0;
	id = 0;
	value = "";
	circleSelect = false;
	quadSelect = false;
	rectSelect = false;
}

Figura::~Figura()
{

}

void Figura::circle(lua_State * file) 
{
	lua_getglobal(file, "circle");

	if (lua_pcall(file, 0, 3, 0) != 0) 
	{
		cout << "Error en funcion" << lua_tostring(file, -1) << endl;
	}

	x = (int)lua_tonumber(file, -3);
	y = (int)lua_tonumber(file, -2);
	radius = (int)lua_tonumber(file, -1);
	lua_pop(file, 1);
}

void Figura::quad(lua_State * file)
{
	lua_getglobal(file, "quad");

	if (lua_pcall(file, 0, 4, 0) != 0) 
	{
		cout << "Error en funcion" << lua_tostring(file, -1) << endl;
	}

	x = (int)lua_tonumber(file, -4);
	y = (int)lua_tonumber(file, -3);
	width = (int)lua_tonumber(file, -2);
	height = (int)lua_tonumber(file, -1);
	lua_pop(file, 1);
}

void Figura::secondQuad(lua_State * file)
{
	lua_getglobal(file, "secondQuad");

	if (lua_pcall(file, 0, 4, 0) != 0) 
	{
		cout << "Error en funcion" << lua_tostring(file, -1) << endl;
	}

	x = (int)lua_tonumber(file, -4);
	y = (int)lua_tonumber(file, -3);
	width = (int)lua_tonumber(file, -2);
	height = (int)lua_tonumber(file, -1);
	lua_pop(file, 1);
}

int Figura::hash(char * clave)
{
	int hashNum = 0;

	for (int i = 0; i < strlen(clave); i++) 
	{
		hashNum += int(clave[i]) * (i + 1);
	}

	return hashNum;
}

void Figura::start(string _figura, char * _value)
{
	value = _value;
	figura = _figura;
	id = hash(value);
	circleSelect = false;
	quadSelect = false;
	rectSelect = false;

	if (figura == "circulo") 
	{
		lua_State *file = luaL_newstate();
		luaopen_base(file);

		if (luaL_loadfile(file, "coords.lua") || lua_pcall(file, 0, 0, 0)) 
		{
			cout << "Error al cargar archivo" << endl;
		}

		circle(file);
		lua_close(file);
	}
	else if (figura == "cuadrado") {
		lua_State *file = luaL_newstate();
		luaopen_base(file);

		if (luaL_loadfile(file, "coords.lua") || lua_pcall(file, 0, 0, 0)) {
			cout << "Error al cargar archivo" << endl;
		}

		quad(file);
		lua_close(file);
	}
	else if (figura == "rectangulo") {
		lua_State *file = luaL_newstate();
		luaopen_base(file);

		if (luaL_loadfile(file, "coords.lua") || lua_pcall(file, 0, 0, 0)) {
			cout << "Error al cargar archivo" << endl;
		}

		secondQuad(file);
		lua_close(file);
	}
	else {
		cout << "Figura incorrecta" << endl;
	}
}

void Figura::draw(){
	if (figura == "circulo") {
		if (circleSelect) {
			ofSetColor(0, 206, 209);
			ofDrawCircle(x, y, radius);
			ofSetColor(255, 255, 255);
			ofDrawBitmapString("ID: " + ofToString(id) + "  Value: " + value, 450, 50);
		}
		else {
			ofSetColor(255, 255, 255);
			ofDrawCircle(x, y, radius);
		}
	}
	else if (figura == "cuadrado") {
		if (quadSelect) {
			ofSetColor(139, 0, 0);
			ofDrawRectangle(x, y, width, height);
			ofSetColor(255, 255, 255);
			ofDrawBitmapString("ID: " + ofToString(id) + "  Value: " + value, 450, 50);
		}
		else {
			ofSetColor(255, 255, 255);
			ofDrawRectangle(x, y, width, height);
		}
	}
	else if (figura == "rectangulo") {
		if (rectSelect) {
			ofSetColor(173, 255, 47);
			ofDrawRectangle(x, y, width, height);
			ofSetColor(255, 255, 255);
			ofDrawBitmapString("ID: " + ofToString(id) + "  Value: " + value, 450, 50);
		}
		else {
			ofSetColor(255, 255, 255);
			ofDrawRectangle(x, y, width, height);
		}
	}
}
