#include <iostream>
#include <fstream>

char numberArray[4096];
std::ofstream archivoEntrada;

void pushNumbers() {

	archivoEntrada.open("Array.txt", std::ios::out);
	
	while (true)
	{
		int indexArray, numero;
		std::cout << "Introduce el indice en el que quieres meter el numero: \n";
		std::cin >> indexArray;
		if (indexArray < 0)
			break;
		std::cout << "Introduce los numeros que quieras meter al Array,\n Agrega un numero negativo para indicar el final \n";
		std::cin >> numero;
		if (numero < 0)
			break;
		else
			numero -= 1;

		archivoEntrada << "Indice de array: " << indexArray << "\nElemento: " << numero << "\n";
		numero = numero % 8;
		numberArray[indexArray] = numberArray[indexArray] | (1 << (numero));
	}
	archivoEntrada.close();
}

void searchNumbers() {
	while (true)
	{
		int indexArray, numero;
		std::cout << "Introduce el indice en el que quieres busca el valor: \n";
		std::cin >> indexArray;
		if (indexArray < 0)
			break;
		std::cout << "Introduce el numero que quieres buscar dentro del Array,\n";
		std::cin >> numero;
		if (numero < 0)
			break;
		else
			numero -= 1;

		numero = numero % 8;
		if ((numberArray[indexArray] & (1 << (numero))))
			std::cout << "El numero si esta dentro del Array.\n";
		else
			std::cout << "El numero no esta dentro del Array.\n";
	}
}

int main(int argv, char** argc) {

	pushNumbers();
	searchNumbers();

	system("Pause");
	return 0;
}