#pragma once
#include <iostream>

class Player
{

private:
	int x, y;

public:
	Player();
	~Player();
	void setPosition(int _x, int _y);
	int getX();
	int getY();

};

class vec2
{
public:
	vec2();
	~vec2();
	vec2(int _x, int _y);
	int x, y;
};
