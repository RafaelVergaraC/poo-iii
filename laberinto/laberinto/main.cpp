#include <iostream>
#include "Player.h"
#include <stack>

std::stack<vec2> visitados;
enum casilla
{
	pared = 0,
	camino,
	salida,
	visitado,
};
int map[10][10]
{
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
	1,1,1,1,1,1,1,1,1,2,
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
};
int mapvisitado[10][10]
{
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
	1,1,1,1,1,1,1,1,1,2,
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,
};


bool checkUp(int y, int x)
{
	if (map[y + 1][x] == camino && mapvisitado[y + 1][x] != visitado)
		return true;

	else
		return false;
}
bool checkDown(int y, int x)
{
	if (map[y - 1][x] == camino && mapvisitado[y - 1][x] != visitado)
		return true;

	else
		return false;
}
bool checkRight(int y, int x)
{
	if (map[y][x + 1] == camino && mapvisitado[y][x + 1] != visitado)
		return true;

	else
		return false;
}
bool checkLeft(int y, int x)
{
	if (map[y][x - 1] == camino && mapvisitado[y][x - 1] != visitado)
		return true;

	else
		return false;
}

bool checkEnd(int y, int x)
{
	if (map[y][x] == salida)
		return true;

	else
		return false;
}

int main()
{
	Player player;

	player.setPosition(5, 0);

	while (true)
	{



		if (map[player.getY()][player.getX()] == camino)
		{

			if (checkUp(player.getY(), player.getX()))
			{
				visitados.push(vec2(player.getX(), player.getY()));
				mapvisitado[player.getY()][player.getX()] = 3;
				player.setPosition(player.getY() + 1, player.getX());
			}
			else if (checkRight(player.getY(), player.getX()))
			{
				visitados.push(vec2(player.getX(), player.getY()));
				mapvisitado[player.getY()][player.getX()] = 3;
				player.setPosition(player.getY(), player.getX() + 1);
			}
			else if (checkDown(player.getY(), player.getX()))
			{
				visitados.push(vec2(player.getX(), player.getY()));
				mapvisitado[player.getY()][player.getX()] = 3;
				player.setPosition(player.getY() - 1, player.getX());
			}
			else if (checkLeft(player.getY(), player.getX()))
			{
				visitados.push(vec2(player.getX(), player.getY()));
				mapvisitado[player.getY()][player.getX()] = 3;
				player.setPosition(player.getY(), player.getX() - 1);
			}
			else if (checkEnd(player.getY(), player.getX()))
			{
				std::cout << "Encontro la salida \n";
				break;
			}
			else
			{
				player.setPosition(visitados.top().y, visitados.top().x);
				visitados.pop();
			}
		}



	}

	system("pause");
	return 0;
}