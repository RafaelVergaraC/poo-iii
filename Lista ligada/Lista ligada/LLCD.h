#pragma once
#include <iostream>

class LLCD
{
public:
	class Item
	{
	public:
		Item *next;
		Item *prev;
		std::string name;
		Item(std::string n)
		{
			name = n;
		}
	};

	Item* getHeader() { return header; }
	void insertItem(Item* nuevo);
	void insertString(std::string);

	void remove();
	void prev()
	{
		header = header->prev;
	}
	void next()
	{
		header = header->next;
	}

	void print();
	LLCD();
	~LLCD();

private:
	Item *header;
};